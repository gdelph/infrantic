package dnet.gamestudios.infrantic;

import com.badlogic.gdx.backends.android.AndroidApplication;

import android.os.Bundle;

public class AndroidGame extends AndroidApplication {

	private Infrantic infrantic;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_android_game);
		infrantic = new Infrantic(true);
		initialize(infrantic, false);
	}

	@Override
	protected void onDestroy() {
		infrantic.dispose();
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		infrantic.pause();
		super.onPause();
	}

	@Override
	protected void onResume() {
		infrantic.resume();
		super.onResume();
	}
	
	
}
