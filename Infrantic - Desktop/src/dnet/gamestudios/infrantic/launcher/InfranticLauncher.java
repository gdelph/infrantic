package dnet.gamestudios.infrantic.launcher;

import java.awt.Dimension;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.UIManager;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.launcher.gui.LauncherWindow;

public class InfranticLauncher {

	private final boolean useLauncher = false;
	public static int width = 856;
	public static int height = 482;
	
	private LauncherWindow window;
	
	public InfranticLauncher() {
		if (useLauncher) {
			showGui();
		} else {
			launchGame();
		}
	}
	
	public void showGui() {
		window = new LauncherWindow(this);
		window.setPreferredSize(new Dimension(512, 512));
		window.setTitle("Infrantic Launcher");
		window.validate();
		window.pack();
		window.setLocationRelativeTo(null);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		try {
			window.setIconImage(ImageIO.read(InfranticLauncher.class.getResourceAsStream("ic_launcher.png")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		window.setVisible(true);
	}
	
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			new InfranticLauncher();
		}
	}
	
	public void launchGame() {
		Infrantic infrantic = new Infrantic(false);
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Infrantic: " + infrantic.VERSION;
		cfg.useGL20 = false;
		cfg.width = width;
		cfg.height = height;
		//cfg.resizable = false;
		cfg.addIcon("dnet/gamestudios/infrantic/launcher/ic_launcher.png", Files.FileType.Classpath);
		if (window != null) {
			window.setVisible(false);
			window.dispose();
		}
		new LwjglApplication(infrantic, cfg);
		
	}
}
