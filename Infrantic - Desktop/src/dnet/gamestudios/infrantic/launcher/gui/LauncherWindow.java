package dnet.gamestudios.infrantic.launcher.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import dnet.gamestudios.infrantic.launcher.InfranticLauncher;

public class LauncherWindow extends JFrame {

	private static final long serialVersionUID = 2508588318661529779L;
	private InfranticLauncher launcher;
	private final String DOCUMENT_URL = "";
	private JTextPane jtp;
	
	public LauncherWindow(InfranticLauncher launcher) {
		this.launcher = launcher;
		setLayout(new BorderLayout());
		JButton bStart = new JButton("Play Infrantic");
		add(bStart, BorderLayout.SOUTH);
		bStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				LauncherWindow.this.launcher.launchGame();
			}
			
		});
		jtp = new JTextPane();
		jtp.setEditable(false);
		JScrollPane jsp = new JScrollPane();
		jsp.setViewportView(jtp);
		add(jsp, BorderLayout.CENTER);
		try {
			jtp.setPage(DOCUMENT_URL);
		} catch (IOException e) {
			e.printStackTrace();
			jtp.setText("Failed to load, Are you connected to the internet?");
		}
	}
}
