package dnet.gamestudios.infrantic.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.utils.IntIntMap;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.gui.component.GuiComponent;
import dnet.gamestudios.infrantic.gui.touchcontrols.GuiTouchControls;
import dnet.gamestudios.infrantic.state.game.GameState;
import dnet.gamestudios.infrantic.state.game.map.MapCollisionHelper;

public class FPCameraController extends InputAdapter {

	private Infrantic infrantic;
	private final Camera camera;
	private int controlPointer = -1;
	private int dragPointer = -2;
	private final IntIntMap keys = new IntIntMap();
	private int STRAFE_LEFT = Keys.A;
	private int STRAFE_RIGHT = Keys.D;
	private int FORWARD = Keys.W;
	private int BACKWARD = Keys.S;
	private int UP = Keys.Q;
	private int DOWN = Keys.E;
	private float velocity = 5;
	private float degreesPerPixel = 0.2f; // 0.5f
	private final Vector3 tmp = new Vector3();
	
	public GuiTouchControls touchOverlay;
	private GuiComponent touchedComponent = null;
	private ControlMode controlMode = ControlMode.ON_FOOT;
	private MapCollisionHelper collisionHelper;
	
	public FPCameraController(Infrantic infrantic, Camera camera) {
		this.infrantic = infrantic;
		this.camera = camera;
		this.collisionHelper = new MapCollisionHelper(infrantic);
	}
	
	public void setTouchControls(GuiTouchControls touchControls) {
		this.touchOverlay = touchControls;
	}
	
	@Override
	public boolean keyDown (int keycode) {
		keys.put(keycode, keycode);
		return true;
	}

	@Override
	public boolean keyUp (int keycode) {
		keys.remove(keycode, 0);
		return true;
	}
	
	public Ray getRay() {
		return camera.getPickRay(infrantic.graphics.getWidth() / 2, infrantic.graphics.getHeight() / 2);
	}
	
	/**
	 * Sets the velocity in units per second for moving forward, backward and strafing left/right.
	 * @param velocity the velocity in units per second
	 */
	public void setVelocity(float velocity) {
		this.velocity = velocity;
	}
	
	/**
	 * Sets how many degrees to rotate per pixel the mouse moved.
	 * @param degreesPerPixel
	 */
	public void setDegreesPerPixel(float degreesPerPixel) {
		this.degreesPerPixel = degreesPerPixel;
	}
	
	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		if (infrantic.isMobileVersion() == false) return true;
		if (this.controlPointer == pointer) return false;
		this.dragPointer = pointer;
		float deltaX = -Gdx.input.getDeltaX(pointer) * degreesPerPixel;
		float deltaY = -Gdx.input.getDeltaY(pointer) * degreesPerPixel;
		camera.direction.rotate(camera.up, deltaX);
		tmp.set(camera.direction).crs(camera.up).nor();
		camera.direction.rotate(tmp, deltaY);
		return true;
	}

	private boolean nonMobileDrag() {
		if (!Gdx.input.isCursorCatched()) return false;
		float deltaX = -Gdx.input.getDeltaX(0) * degreesPerPixel;
		float deltaY = -Gdx.input.getDeltaY(0) * degreesPerPixel;
		camera.direction.rotate(camera.up, deltaX);
		tmp.set(camera.direction).crs(camera.up).nor();
		camera.direction.rotate(tmp, deltaY);
		return true;
	}
	
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if (controlPointer == -1) {
			if (pointer == controlPointer) return false;
			if (touchOverlay != null) { 
				touchedComponent = touchOverlay.isComponent(screenX, Gdx.graphics.getHeight() - screenY);
				if (touchedComponent != null) {
					controlPointer = pointer;
					touchOverlay.onComponentClicked(this, touchedComponent);
					return true;
				}
			}
		}
		return super.touchDown(screenX, screenY, pointer, button);
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if (pointer == controlPointer) {
			if (touchOverlay != null) {
				touchOverlay.onComponentReleased(this, touchedComponent);
			}
			controlPointer = -1;
			return true;
		}
		if (pointer == dragPointer) {
			dragPointer = -2;
		}
		return super.touchUp(screenX, screenY, pointer, button);
	}

	public void update() {
		update(Gdx.graphics.getDeltaTime());
	}
	
	public void update(float deltaTime) {
		if (infrantic.getScreen() instanceof GameState) {
			if (!infrantic.isMobileVersion()) {
				nonMobileDrag();
			}
			GameState state = (GameState) infrantic.getScreen();
			if(keys.containsKey(FORWARD)) {
				tmp.set(camera.direction).nor().scl(deltaTime * velocity);
				tmp.y = 0;
				if (collisionHelper.canMove(MapCollisionHelper.FORWARD, state)) {
					camera.position.add(tmp);
				}
			} else if(keys.containsKey(BACKWARD)) {
				tmp.set(camera.direction).nor().scl(-deltaTime * velocity);
				tmp.y = 0;
				camera.position.add(tmp);
			} else if(keys.containsKey(STRAFE_LEFT)) {
				tmp.set(camera.direction).crs(camera.up).nor().scl(-deltaTime * velocity);
				camera.position.add(tmp);
			} else if(keys.containsKey(STRAFE_RIGHT)) {
				tmp.set(camera.direction).crs(camera.up).nor().scl(deltaTime * velocity);
				camera.position.add(tmp);
			}
			if(keys.containsKey(UP)) {
				tmp.set(camera.up).nor().scl(deltaTime * velocity);
				camera.position.add(tmp);
			}
			if(keys.containsKey(DOWN)) {
				tmp.set(camera.up).nor().scl(-deltaTime * velocity);
				camera.position.add(tmp);
			}
			if (infrantic.gameGui.hasFocus()) return;
			camera.update(true);
			infrantic.localPlayer.setPosition(camera.position.x, camera.position.y, camera.position.z);
			if (touchOverlay != null) {
				if (controlPointer != -1) {
					int x = Gdx.input.getX(controlPointer);
					int y = Gdx.input.getY(controlPointer);
					GuiComponent temp = touchOverlay.isComponent(x, Gdx.graphics.getHeight() - y);
					if (!touchedComponent.equals(temp) && temp != null) {
						touchOverlay.onComponentReleased(this, touchedComponent);
						touchedComponent = temp;
						temp = null;
						touchOverlay.onComponentClicked(this, touchedComponent);
					}
				}
			}
		}
	}
}
