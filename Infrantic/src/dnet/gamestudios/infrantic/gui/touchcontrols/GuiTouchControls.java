package dnet.gamestudios.infrantic.gui.touchcontrols;

import dnet.gamestudios.infrantic.gui.component.GuiComponent;
import dnet.gamestudios.infrantic.input.FPCameraController;

public interface GuiTouchControls {
	
	public void render();
	public void onComponentClicked(FPCameraController cameraController, GuiComponent component);
	public void onComponentReleased(FPCameraController cameraController, GuiComponent component);
	public GuiComponent isComponent(int x, int y);
}
