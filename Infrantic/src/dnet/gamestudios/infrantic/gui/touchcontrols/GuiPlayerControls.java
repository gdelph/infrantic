package dnet.gamestudios.infrantic.gui.touchcontrols;

import java.util.ArrayList;

import com.badlogic.gdx.Input.Keys;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.gui.component.Button;
import dnet.gamestudios.infrantic.gui.component.GuiComponent;
import dnet.gamestudios.infrantic.input.FPCameraController;

public class GuiPlayerControls implements GuiTouchControls {

	private Infrantic infrantic;
	private ArrayList<GuiComponent> components;
	private Button bUp, bDn, bL, bR;
	private int arrowsX = 0;
	private int arrowsY = 0;
	private int arrowScale = 50;
	
	public GuiPlayerControls(Infrantic infrantic) {
		this.infrantic = infrantic;
		arrowScale = (infrantic.graphics.getWidth() * infrantic.graphics.getHeight()) / arrowScale;
		components = new ArrayList<GuiComponent>();
		bUp = new Button(infrantic, 30, 30, 50, 50, "texture/menu/touchcont/bup.png", "");
		bDn = new Button(infrantic, 30, 30, 50, 50, "texture/menu/touchcont/bdn.png", "");
		bL = new Button(infrantic, 30, 30, 50, 50, "texture/menu/touchcont/bl.png", "");
		bR = new Button(infrantic, 30, 30, 50, 50, "texture/menu/touchcont/br.png", "");
		components.add(bUp);
		components.add(bDn);
		components.add(bL);
		components.add(bR);
	}
	
	@Override
	public void render() {
		bR.setPosition(arrowsX + 80, arrowsY + 50);
		bUp.setPosition(arrowsX + 45, arrowsY + 95);
		bDn.setPosition(arrowsX + 45, arrowsY + 4);
		bL.setPosition(arrowsX + 10, arrowsY + 50);
		for (int i = 0; i < components.size(); i ++) {
			components.get(i).render(infrantic.graphics);
		}
	}

	@Override
	public void onComponentClicked(FPCameraController cameraController, GuiComponent component) {
		if (component.equals(bUp)) { 
			cameraController.keyDown(Keys.W);
		} else if (component.equals(bDn)) { 
			cameraController.keyDown(Keys.S);
		} else if (component.equals(bL)) { 
			cameraController.keyDown(Keys.A);
		} else if (component.equals(bR)) { 
			cameraController.keyDown(Keys.D);
		} else {
			component.onClick();
		}
	}
	
	@Override
	public void onComponentReleased(FPCameraController cameraController, GuiComponent component) {
		if (component.equals(bUp)) { 
			cameraController.keyUp(Keys.W);
		} else if (component.equals(bDn)) { 
			cameraController.keyUp(Keys.S);
		} else if (component.equals(bL)) { 
			cameraController.keyUp(Keys.A);
		} else if (component.equals(bR)) { 
			cameraController.keyUp(Keys.D);
		} else {
			component.onRelease();
		}
	}
	
	@Override
	public GuiComponent isComponent(int x, int y) {
		for (int i = 0; i < components.size(); i ++) {
			if (components.get(i).containsPoint(x, y)) {
				return components.get(i);
			}
		}
		return null;
	}

}
