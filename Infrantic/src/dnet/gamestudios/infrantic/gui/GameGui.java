package dnet.gamestudios.infrantic.gui;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.graphics.Graphics;

public class GameGui {
	private CrossHair crosshair;
	private Map<String, GuiOverlay> guis;
	private GuiOverlay guiWithFocus = null;
	
	public GameGui(Infrantic infrantic) {
		guis = new HashMap<String, GuiOverlay>();
		crosshair = new CrossHair(infrantic);
		crosshair.load();
	}
	
	public void addGui(String id, GuiOverlay gui) {
		if (!containsGui(id)) {
			gui.load();
			guis.put(id, gui);
			if (gui.takesFocus) {
				this.guiWithFocus = gui;
				if (Gdx.input.isCursorCatched()) {
					Gdx.input.setCursorCatched(false);
				}
				crosshair.setVisible(false);
			}
		} else {
			gui.load();
			guis.put(id, gui);
			if (gui.takesFocus) {
				this.guiWithFocus = gui;
				if (Gdx.input.isCursorCatched()) {
					Gdx.input.setCursorCatched(false);
				}
				crosshair.setVisible(false);
			}
		}
	}
	
	public void removeGui(String id) {
		GuiOverlay temp;
		if (containsGui(id)) {
			temp = guis.get(id);
			guis.remove(id);
			if (guiWithFocus != null) {
				if (guiWithFocus.equals(temp)) {
					guiWithFocus = null;
					if (!Gdx.input.isCursorCatched()) {
						Gdx.input.setCursorCatched(true);
					}
					crosshair.setVisible(true);
				}
			}
			temp.dispose();
		}
	}
	
	public GuiOverlay getGui(String id) {
		if (id.equals("Corsshair")) return this.crosshair;
		if (containsGui(id)) {
			return guis.get(id);
		} else {
			return null;
		}
	}
	
	private boolean containsGui(String id) {
		return guis.containsKey(id);
	}
	
	public void update(float delta) {
		String[] keys = keySet();
		for (int i = 0; i < keys.length; i ++) {
			guis.get(keys[i]).update(delta);
		}
	}
	
	public void render(Graphics graphicsEngine) {
		crosshair.setVisible(true);
		crosshair.render(graphicsEngine);
		String[] keys = keySet();
		for (int i = 0; i < keys.length; i ++) {
			guis.get(keys[i]).render(graphicsEngine);
		}
	}
	
	public void onResize(int width, int height) {
		crosshair.onResize(width, height);
		String[] keys = keySet();
		for (int i = 0; i < keys.length; i ++) {
			guis.get(keys[i]).onResize(width, height);
		}
	}
	
	public boolean hasFocus() {
		if (guiWithFocus == null) return false;
		return true;
	}
	
	private String[] keySet() {
		return guis.keySet().toArray(new String[0]);
	}
	
	public void dispose() {
		crosshair.dispose();
		String[] keys = keySet();
		for (int i = 0; i < keys.length; i ++) {
			guis.get(keys[i]).dispose();
		}
	}
	
}
