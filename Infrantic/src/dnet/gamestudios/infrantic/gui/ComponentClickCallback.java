package dnet.gamestudios.infrantic.gui;

public interface ComponentClickCallback {

	public boolean onClick(String param);
	
}
