package dnet.gamestudios.infrantic.gui.component;

import com.badlogic.gdx.math.Rectangle;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.graphics.Graphics;

/**
 * @author Guy Delph
 *
 */
public abstract class GuiComponent {

	/**
	 * The bounds of this component 
	 */
	protected Rectangle bounds;
	protected boolean visible = false;
	protected Infrantic infrantic;
	
	public GuiComponent(Infrantic infrantic, float x, float y, float width, float height) {
		bounds = new Rectangle(x, y, width, height);
		this.infrantic = infrantic;
	}
	
	/**
	 * Check if the components bounds contains the given point
	 * @param x x Coordinate of the point
	 * @param y y Coordinate of the point
	 * @return true if the components bounds contains the point, false otherwise
	 */
	public boolean containsPoint(float x, float y) {
		return this.bounds.contains(x, y);
	}
	
	/**
	 * Set the position of this component
	 * @param x the new x position
	 * @param y the new y position
	 */
	public void setPosition(float x, float y) {
		this.bounds.setPosition(x, y);
	}
	
	/**
	 * Set the size of this component
	 * @param width the new width
	 * @param height the new height
	 */
	public void setSize(float width, float height) {
		this.bounds.setSize(width, height);
	}
	
	/**
	 * Get the x position of this component
	 * @return the x position of this component
	 */
	public float getX() {
		return this.bounds.getX();
	}
	
	/**
	 * Get the y position of this component
	 * @return the y position of this component
	 */
	public float getY() {
		return this.bounds.getY();
	}
	
	/**
	 * Get the width of this component
	 * @return the width of this component
	 */
	public float getWidth() {
		return this.bounds.getWidth();
	}
	
	/**
	 * Get the height of this component
	 * @return the height of this component
	 */
	public float getHeight() {
		return this.bounds.getHeight();
	}
	
	/**
	 * Set if this component should be drawn
	 * @param visible true if this component should be drawn, false otherwise
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	/**
	 * Is this component visible
	 * @return true if this component is visible, false otherwise
	 */
	public boolean isVisible() {
		return this.visible;
	}
	
	/**
	 * Updates this component
	 */
	public abstract void update(float delta);
	/**
	 * Render this component
	 */
	public abstract void render(Graphics graphics);
	/**
	 * Dispose of this component and its resources
	 */
	public abstract void dispose();
	/**
	 * Called when the mouse is over the component
	 */
	public void onHover() {
		
	}
	/**
	 * Called when the component is clicked
	 */
	public void onClick() {
		
	}
	
	public void onRelease() {
		
	}
}
