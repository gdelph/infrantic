package dnet.gamestudios.infrantic.gui.component;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.graphics.Graphics;
import dnet.gamestudios.infrantic.gui.ComponentClickCallback;

public class StringButton extends GuiComponent {

	private String label;
	private ComponentClickCallback callback;
	
	public StringButton(Infrantic infrantic, float x, float y, String label) {
		super(infrantic, x, y, 0, 0);
		this.label = label;
		
	}
	
	public void setCallback(ComponentClickCallback callback) {
		this.callback = callback;
	}
	
	@Override
	public void update(float delta) {
		
	}

	@Override
	public void render(Graphics graphics) {
		graphics.drawString(label, getX(), getY());
	}

	@Override
	public void dispose() {}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public boolean containsPoint(float x, float y) {
		//super.bounds.setPosition(super.getX(), super.getY());
		super.bounds.setSize(infrantic.graphics.getTextBounds(label).width, infrantic.graphics.getTextBounds(label).height);
		if (super.containsPoint(x, y)) {
			if (callback != null) {
				callback.onClick(label);
				System.out.print("click");
			}
		}
		return super.containsPoint(x, y);
	}
	
	
}
