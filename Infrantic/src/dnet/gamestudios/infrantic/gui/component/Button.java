package dnet.gamestudios.infrantic.gui.component;

import com.badlogic.gdx.graphics.Texture;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.graphics.Graphics;

public class Button extends GuiComponent {

	private String background;
	private String label;
	
	public Button(Infrantic infrantic, float x, float y, float width, float height, String backgroundPath, String label) {
		super(infrantic, x, y, width, height);
		infrantic.assetManager.load(backgroundPath, Texture.class);
		this.background = backgroundPath;
		this.label = label;
	}

	@Override
	public void update(float delta) {
		
	}

	@Override
	public void render(Graphics graphics) {
		graphics.globalSpriteBatch.begin();
		graphics.globalSpriteBatch.setProjectionMatrix(graphics.camera2D.combined);
		graphics.globalSpriteBatch.draw(infrantic.assetManager.get(background, Texture.class), getX(), getY(), getWidth(), getHeight());
		graphics.globalSpriteBatch.end();
		graphics.drawString(label, (getX() + (getWidth() /2) - (graphics.getTextBounds(label).width / 2)), (getY() + (getHeight() / 2)) + (graphics.getTextBounds(label).height / 2));
	}

	@Override
	public void dispose() {
		infrantic.assetManager.unload(background);
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
}
