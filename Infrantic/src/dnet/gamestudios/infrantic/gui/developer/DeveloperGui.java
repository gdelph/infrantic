package dnet.gamestudios.infrantic.gui.developer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.graphics.Graphics;
import dnet.gamestudios.infrantic.gui.ComponentClickCallback;
import dnet.gamestudios.infrantic.gui.GuiOverlay;
import dnet.gamestudios.infrantic.gui.component.StringButton;

public class DeveloperGui extends GuiOverlay implements ComponentClickCallback {

	private String textureBG = "texture/grey.png";
	private StringButton bFPS;
	
	public DeveloperGui(Infrantic infrantic, int x, int y, int width, int height) {
		super(infrantic, x, y, width, height);
		super.setTakesFocus(true);
	}

	@Override
	public void load() {
		infrantic.assetManager.load(textureBG, Texture.class);
		bFPS = new StringButton(infrantic, 25, height - 10, "Toggle FPS");
		bFPS.setCallback(this);
	}
	
	@Override
	public void onResize(int width, int height) {
		super.width = Gdx.graphics.getWidth() - 40;
		super.height = Gdx.graphics.getHeight() - 50;
		bFPS.setPosition(25, super.height - 10);
	}

	@Override
	public void render(Graphics graphics) {
		if (!infrantic.assetManager.isLoaded(textureBG)) return;
		graphics.globalSpriteBatch.begin();
		graphics.globalSpriteBatch.setProjectionMatrix(graphics.camera2D.combined);
		graphics.globalSpriteBatch.draw(infrantic.assetManager.get(textureBG, Texture.class), x, y, width, height);
		graphics.globalSpriteBatch.end();
		graphics.drawString("Developer Menu Press F9 to close", 25f, height + 19, Color.BLACK);
		bFPS.render(graphics);
	}

	@Override
	public void update(float delta) {
		float x = Gdx.input.getX();
		float y = Gdx.graphics.getHeight() - Gdx.input.getY();
		bFPS.containsPoint(x, y);
	}

	@Override
	public void dispose() {
		infrantic.assetManager.unload(textureBG);
	}

	@Override
	public boolean onClick(String param) {
		if (param.equals("Toggle FPS")) {
			System.out.println("Click");
		}
		return false;
	}

	
}
