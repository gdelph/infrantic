package dnet.gamestudios.infrantic.gui;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.graphics.Graphics;

public abstract class GuiOverlay {

	protected Infrantic infrantic;
	protected int x;
	protected int y;
	protected int width;
	protected int height;
	protected boolean movable;
	protected boolean visible = false;
	protected boolean takesFocus = false;
	
	public GuiOverlay(Infrantic infrantic, int x, int y, int width, int height) {
		this.infrantic = infrantic;
		this.x = x;
		this.y = y;
		this.width = width;
		this. height= height;
	}
	
	public abstract void onResize(int width, int height);
	public abstract void render(Graphics graphicsEngine);
	public abstract void update(float delta);
	public abstract void load();
	public abstract void dispose();

	public Infrantic getInfrantic() {
		return infrantic;
	}

	public void setInfrantic(Infrantic infrantic) {
		this.infrantic = infrantic;
	}

	public float getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public boolean isMovable() {
		return movable;
	}

	public void setMovable(boolean movable) {
		this.movable = movable;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isTakesFocus() {
		return takesFocus;
	}

	public void setTakesFocus(boolean takesFocus) {
		this.takesFocus = takesFocus;
	}
	
}
