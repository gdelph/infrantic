package dnet.gamestudios.infrantic.gui;

import com.badlogic.gdx.graphics.Texture;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.graphics.Graphics;

public class CrossHair extends GuiOverlay {

	private String texture = "texture/crosshair.png";
	
	public CrossHair(Infrantic infrantic) {
		super(infrantic, infrantic.graphics.getWidth() / 2, infrantic.graphics.getHeight() / 2, 16, 16);
	}

	@Override
	public void update(float delta) {
		
	}

	@Override
	public void render(Graphics graphics) {
		if (!visible) return;
		graphics.globalSpriteBatch.begin();
		graphics.globalSpriteBatch.setProjectionMatrix(graphics.camera2D.combined);
		graphics.globalSpriteBatch.draw(infrantic.assetManager.get(texture, Texture.class), x, y);
		graphics.globalSpriteBatch.end();
	}

	@Override
	public void dispose() {
		infrantic.assetManager.unload(texture);
	}

	@Override
	public void onResize(int width, int height) {
		super.setX((width / 2) - 8);
		super.setY((height / 2) - 8);
	}

	@Override
	public void load() {
		infrantic.assetManager.load(texture, Texture.class);
	}

}
