package dnet.gamestudios.infrantic.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;

import dnet.gamestudios.infrantic.graphics.Graphics;
import dnet.gamestudios.infrantic.graphics.model.entity.CubicMarkerModel;

public class EntityCubicMarker extends Entity {

	private int direction = 0;
	private float heightModifier = 0;
	private float angle = 0;
	private float r = 1;
	private float g = 1;
	private float b = 1;
	
	public EntityCubicMarker(long entityId, float x, float y, float z) {
		super(entityId, new CubicMarkerModel(null, x, y, z, 1, 1, 1));
		model.load();
	}
	
	public EntityCubicMarker(long entityId, float x, float y, float z, float r, float g, float b) {
		super(entityId, new CubicMarkerModel(null, x, y, z, 1, 1, 1));
		model.load();
		setColor(r, g, b);
	}
	
	public void setColor(Color color) {
		this.r = color.r;
		this.g = color.g;
		this.b = color.b;
	}
	
	public void setColor(float[] color) {
		if (color == null) return;
		if (color.length != 3) return;
		this.r = color[0];
		this.g = color[1];
		this.b = color[2];
	}
	
	public void setColor(float r, float g, float b) {
		this.r = r;
		this.g = g;
		this.b = b;
	}
	
	@Override
	public void render(Graphics graphics) {
		Gdx.gl10.glColor4f(r, g, b, 1);
		super.render(graphics);
		Gdx.gl10.glColor4f(1, 1, 1, 1);
	}

	@Override
	public void update(float delta) {
		angle += delta * 500;
		if (angle > 360) {
			angle = 0;
		}
		model.setRotation(0, angle, 0);
		if (direction == 0) {
			heightModifier += delta * 0.3f;
			if (heightModifier > 0.5f) {
				direction = 1;
			}	
		} else {
			heightModifier -= delta * 0.3f;
			if (heightModifier < 0) {
				direction = 0;
			}
		}
		
			
			
		model.setHeight(1 + heightModifier);
	}

}
