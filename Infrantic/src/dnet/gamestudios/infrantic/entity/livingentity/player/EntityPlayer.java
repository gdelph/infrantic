package dnet.gamestudios.infrantic.entity.livingentity.player;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.entity.livingentity.LivingEntity;
import dnet.gamestudios.infrantic.graphics.Graphics;
import dnet.gamestudios.infrantic.graphics.model.entity.PlayerModel;

public class EntityPlayer extends LivingEntity {

	private Infrantic infrantic;
	
	public EntityPlayer(Infrantic infrantic, long entityId, float x, float y, float z) {
		super(entityId, new PlayerModel(x, y, z));
		this.infrantic = infrantic;
		putAt(x, y, z);
	}

	public void putAt(float x, float y, float z) {
		infrantic.graphics.camera3D.position.set(x, y + 1.8f, z);
		infrantic.graphics.camera3D.update();
		setPosition(x, y, z);
	}
	
	@Override
	public void update(float delta) {
		
	}

	@Override
	public void render(Graphics graphicsEngine) {
	
	}
	
	
}
