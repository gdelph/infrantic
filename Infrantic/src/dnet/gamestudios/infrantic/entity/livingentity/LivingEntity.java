package dnet.gamestudios.infrantic.entity.livingentity;

import dnet.gamestudios.infrantic.entity.Entity;
import dnet.gamestudios.infrantic.graphics.model.Model;

public abstract class LivingEntity extends Entity {

	protected float health;
	protected float maxHealth;
	
	public LivingEntity(long entityId, Model entityModel) {
		super(entityId, entityModel);
	}

	public float getHealth() {
		return health;
	}

	public LivingEntity setHealth(float health) {
		this.health = health;
		return this;
	}

	public float getMaxHealth() {
		return maxHealth;
	}

	public LivingEntity setMaxHealth(float maxHealth) {
		this.maxHealth = maxHealth;
		return this;
	}
	
	
}
