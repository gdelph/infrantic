package dnet.gamestudios.infrantic.entity;

import com.badlogic.gdx.math.collision.BoundingBox;

import dnet.gamestudios.infrantic.graphics.Graphics;
import dnet.gamestudios.infrantic.graphics.model.Model;

public abstract class Entity {
	
	protected long entityId;
	protected Model model;
	
	public Entity(long entityId, Model entityModel) {
		this.model = entityModel;
	}

	public void dispose() {
		model.dispose();
	}
	
	public void setPosition(float x, float y, float z) {
		model.setPosition(x, y, z);
	}
	
	public void setDimensions(float width, float height, float depth) {
		model.setDimensions(width, height, depth);
	}
	
	public float getX() {
		return model.getX();
	}

	public void setX(float x) {
		model.setX(x);
	}

	public float getY() {
		return model.getY();
	}

	public void setY(float y) {
		model.setY(y);
	}

	public float getZ() {
		return model.getZ();
	}

	public void setZ(float z) {
		model.setZ(z);
	}

	public float getWidth() {
		return model.getWidth();
	}

	public void setWidth(float width) {
		model.setWidth(width);
	}

	public float getHeight() {
		return model.getHeight();
	}

	public void setHeight(float height) {
		model.setHeight(height);
	}

	public float getDepth() {
		return model.getDepth();
	}

	public void setDepth(float depth) {
		model.setDepth(depth);
	}
	
	public void render(Graphics graphics) {
		model.render(graphics);
	}
	
	public BoundingBox getBounds() {
		return model.getBounds();
	}
	
	public abstract void update(float delta);
	
	public boolean onColision(Entity entity) {
		return false;
	}
	
	public static boolean insects(Entity entityToTest, Entity entityToTestAgainst) {
		return entityToTest.getBounds().intersects(entityToTestAgainst.getBounds());
	}
	
}
