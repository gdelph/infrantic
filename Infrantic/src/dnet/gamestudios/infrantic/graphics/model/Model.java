package dnet.gamestudios.infrantic.graphics.model;

import com.badlogic.gdx.math.collision.BoundingBox;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.graphics.Graphics;

public abstract class Model {

	protected Infrantic infrantic;
	protected float x;
	protected float y;
	protected float z;
	protected float width;
	protected float height;
	protected float depth;
	protected float rX = 0;
	protected float rY = 0;
	protected float rZ = 0;
	protected BoundingBox bounds;
	
	public Model(Infrantic infrantic, float x, float y, float z, float width, float height, float depth) {
		this.infrantic = infrantic;
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = width;
		this.height = height;
		this.depth = depth;
		bounds = new BoundingBox();
	}
	
	public abstract void load();
	public abstract void render(Graphics graphics);
	public abstract void dispose();

	public Model setPosition(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
		build();
		return this;
	}
	
	public Model setDimensions(float width, float height, float depth) {
		this.width = width;
		this.height = height;
		this.depth = depth;
		build();
		return this;
	}
	
	public void setRotation(float angleX, float angleY, float angleZ) {
		this.rX = angleX;
		this.rY = angleY;
		this.rZ = angleZ;
	}
	
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
		build();
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
		build();
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
		build();
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
		build();
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
		build();
	}

	public float getDepth() {
		return depth;
	}

	public void setDepth(float depth) {
		this.depth = depth;
		build();
	}
	
	public float getStepHeight() {
		return this.x + height;
	}
	
	protected abstract void build();

	public BoundingBox getBounds() {
		return this.bounds;
	}
}
