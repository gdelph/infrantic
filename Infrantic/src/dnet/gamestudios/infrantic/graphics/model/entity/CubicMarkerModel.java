package dnet.gamestudios.infrantic.graphics.model.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.math.Vector3;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.graphics.Graphics;
import dnet.gamestudios.infrantic.graphics.model.Model;

public class CubicMarkerModel extends Model {

	private Mesh meshFront;
	private Mesh meshBack;
	private Mesh meshLeft;
	private Mesh meshRight;
	
	public CubicMarkerModel(Infrantic infrantic, float x, float y, float z, float width, float height, float depth) {
		super(infrantic, x, y, z, width, height, depth);
	}

	@Override
	public void load() {
		if (meshFront == null) {
			meshFront = new Mesh(true, 4, 0, new VertexAttribute(Usage.Position, 3, "b_position"));
		}
		if (meshBack == null) {
			meshBack = new Mesh(true, 4, 0, new VertexAttribute(Usage.Position, 3, "b_position"));
		}
		if (meshLeft == null) {
			meshLeft = new Mesh(true, 4, 0, new VertexAttribute(Usage.Position, 3, "b_position"));
		}
		if (meshRight == null) {
			meshRight = new Mesh(true, 4, 0, new VertexAttribute(Usage.Position, 3, "b_position"));
		}
		build();
	}

	@Override
	public void render(Graphics graphics) {
		Gdx.gl10.glEnable(GL10.GL_LINE_SMOOTH);
		Gdx.gl10.glHint(GL10.GL_LINE_SMOOTH_HINT, GL10.GL_NICEST );
		Gdx.gl10.glPushMatrix();
		Gdx.gl10.glDisable(GL10.GL_TEXTURE_2D);
		Gdx.gl10.glTranslatef(x, y, z);
		Gdx.gl10.glRotatef(rX, 1, 0, 0);
		Gdx.gl10.glTranslatef(width/2 , 0, depth/2);
		Gdx.gl10.glRotatef(rY, 0, 1, 0);
		Gdx.gl10.glRotatef(rZ, 0, 0, 1);
		Gdx.gl10.glTranslatef(-0.5f , 0, -0.5f);
		Gdx.gl10.glLineWidth(1);
		meshFront.render(GL10.GL_LINE_LOOP);
		meshBack.render(GL10.GL_LINE_LOOP);
		meshLeft.render(GL10.GL_LINE_LOOP);
		meshRight.render(GL10.GL_LINE_LOOP);
		Gdx.gl10.glLineWidth(1);
		Gdx.gl10.glEnable(GL10.GL_TEXTURE_2D);
		Gdx.gl10.glTranslatef(0, 0, 0);
		Gdx.gl10.glPopMatrix();
		Gdx.gl10.glDisable(GL10.GL_LINE_SMOOTH);
	}

	@Override
	public void dispose() {
		meshLeft.dispose();
		meshLeft = null;
		meshRight.dispose();
		meshRight = null;
		meshFront.dispose();
		meshFront = null;
		meshBack.dispose();
		meshBack = null;
	}

	@Override
	protected void build() {
		meshFront.setVertices(new float[] {
				0, 			0, 			0,
				0 + width,	0, 			0,
				0 + width,	0 + height, 0,
				0,			0 + height, 0
		});
		meshBack.setVertices(new float[] {
				0 + width, 	0, 			0 + depth,
				0,			0, 			0 + depth,
				0,			0 + height, 0 + depth,
				0 + width,	0 + height, 0 + depth
		});
		meshLeft.setVertices(new float[] {
				0 + width,	0,			0,
				0 + width,	0, 			0 + depth,
				0 + width, 	0 + height,	0 + depth,
				0 + width, 	0 + height, 0
		});
		meshRight.setVertices(new float[] {
				0,			0 + height,	0,
				0,			0 + height, 0 + depth,
				0, 			0,			0 + depth,
				0, 			0, 			0
		});
		bounds.set(new Vector3(x, y, z),  new Vector3(x + width, y + height, z + depth));
	}

}
