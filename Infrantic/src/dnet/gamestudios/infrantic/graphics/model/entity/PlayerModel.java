package dnet.gamestudios.infrantic.graphics.model.entity;

import com.badlogic.gdx.math.Vector3;

import dnet.gamestudios.infrantic.graphics.Graphics;
import dnet.gamestudios.infrantic.graphics.model.Model;

public class PlayerModel extends Model {

	public PlayerModel(float x, float y, float z) {
		super(null, x, y, z, 1, 2, 1);
		build();
	}

	@Override
	public void load() {}

	@Override
	public void render(Graphics graphics) {}

	@Override
	public void dispose() {}

	@Override
	protected void build() {
		bounds.set(new Vector3(x, y, z).sub(0.2f), new Vector3(x + width, y + height, z + depth).add(0.2f));
	}

}
