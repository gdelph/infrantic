package dnet.gamestudios.infrantic.graphics.model.basicparts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.math.Vector3;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.graphics.Graphics;
import dnet.gamestudios.infrantic.graphics.model.Model;

public class ModelWall extends Model {

	private String texture;
	private Mesh meshTop;
	private Mesh meshFront;
	private Mesh meshBack;
	private Mesh meshLeft;
	private Mesh meshRight;
	
	public ModelWall(Infrantic infrantic, String texture, float x, float y, float z, float width, float height, float depth) {
		super(infrantic, x, y, z, width, height, depth);
		this.texture = texture;
	}

	@Override
	public void load() {
		infrantic.assetManager.load(texture, Texture.class);
		if (meshTop == null) {
			meshTop = new Mesh(true, 4, 0, new VertexAttribute(Usage.Position, 3, "b_position"), new VertexAttribute(Usage.TextureCoordinates, 2, "a_texCoords"));
		}
		if (meshFront == null) {
			meshFront = new Mesh(true, 4, 0, new VertexAttribute(Usage.Position, 3, "b_position"), new VertexAttribute(Usage.TextureCoordinates, 2, "a_texCoords"));
		}
		if (meshBack == null) {
			meshBack = new Mesh(true, 4, 0, new VertexAttribute(Usage.Position, 3, "b_position"), new VertexAttribute(Usage.TextureCoordinates, 2, "a_texCoords"));
		}
		if (meshLeft == null) {
			meshLeft = new Mesh(true, 4, 0, new VertexAttribute(Usage.Position, 3, "b_position"), new VertexAttribute(Usage.TextureCoordinates, 2, "a_texCoords"));
		}
		if (meshRight == null) {
			meshRight = new Mesh(true, 4, 0, new VertexAttribute(Usage.Position, 3, "b_position"), new VertexAttribute(Usage.TextureCoordinates, 2, "a_texCoords"));
		}
		build();
	}
	
	@Override
	public void render(Graphics graphics) {
		Gdx.gl10.glPushMatrix();
		Gdx.gl10.glRotatef(rX, 1, 0, 0);
		Gdx.gl10.glRotatef(rY, 0, 1, 0);
		Gdx.gl10.glRotatef(rZ, 0, 0, 1);
		infrantic.assetManager.get(texture, Texture.class).setWrap(TextureWrap.MirroredRepeat, TextureWrap.MirroredRepeat);
		infrantic.assetManager.get(texture, Texture.class);
		infrantic.assetManager.get(texture, Texture.class).bind();
		meshTop.render(GL10.GL_TRIANGLE_FAN);
		meshFront.render(GL10.GL_TRIANGLE_FAN);
		meshBack.render(GL10.GL_TRIANGLE_FAN);
		meshLeft.render(GL10.GL_TRIANGLE_FAN);
		meshRight.render(GL10.GL_TRIANGLE_FAN);
		Gdx.gl10.glPopMatrix();
	}

	@Override
	public void dispose() {
		infrantic.assetManager.unload(texture);
		meshTop.dispose();
		meshTop = null;
		meshLeft.dispose();
		meshLeft = null;
		meshRight.dispose();
		meshRight = null;
		meshFront.dispose();
		meshFront = null;
		meshBack.dispose();
		meshBack = null;
	}

	@Override
	protected void build() {
		meshTop.setVertices(new float[] {
				x + width, 	y + height, z + depth, width, depth,
				x, 			y + height, z + depth, 0, depth,
				x, 			y + height, z,		   0, 0,
				x + width, 	y + height, z,         width, 0,
		});
		meshFront.setVertices(new float[] {
				x, 			y, 			z, 	0,  0,
				x + width,	y, 			z, width, 0,
				x + width,	y + height, z, width, height,
				x,			y + height, z, 0, height
		});
		meshBack.setVertices(new float[] {
				x + width, 	y, 			z + depth, 0,  0,
				x,			y, 			z + depth, width, 0,
				x,			y + height, z + depth, width, height,
				x + width,	y + height, z + depth, 0, height
		});
		meshLeft.setVertices(new float[] {
				x + width,	y,			z, 0, 0,
				x + width,	y, 			z + depth, depth, 0,
				x + width, 	y + height,	z + depth, depth, height,
				x + width, 	y + height, z, 0, height
		});
		meshRight.setVertices(new float[] {
				x,			y + height,	z, 					0, 0,
				x,			y + height, z + depth, 		   	depth, 0,
				x, 			y,			z + depth, 			depth, height,
				x, 			y, 			z, 					0, height
		});
		bounds.set(new Vector3(x, y, z),  new Vector3(x + width, y + height, z + depth));
	}

}
