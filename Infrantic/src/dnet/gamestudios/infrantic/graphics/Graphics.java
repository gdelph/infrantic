package dnet.gamestudios.infrantic.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.state.game.GameState;

public class Graphics {

	public Infrantic infrantic;
	public int width = 0;
	public int height = 0;
	/** The Camera used for 3D */
	public PerspectiveCamera camera3D;
	/** The Camera used for 2D */
	public OrthographicCamera camera2D;
	
	public SpriteBatch globalSpriteBatch;
	private BitmapFont globalFont;
	
	public Graphics(Infrantic infrantic) {
		this.infrantic = infrantic;
		this.width = Gdx.graphics.getWidth();
		this.height = Gdx.graphics.getHeight();
		camera3D = new PerspectiveCamera(45, width, height);
		camera3D.position.set(0, 0, 0);
		camera3D.lookAt(10, 0, 10);
		camera3D.near = 0.1f;
		camera3D.far = 500;
		camera3D.update();
		camera2D = new OrthographicCamera(width, height);
		camera2D.position.set(width / 2, height / 2, 0);
		camera2D.update();
		globalSpriteBatch = new SpriteBatch();
		globalFont = new BitmapFont();
		Gdx.gl10.glEnable(GL10.GL_DEPTH_TEST);
	}

	public void onResize(int width, int height) {
		this.width = width;
		this.height = height;
		camera2D.viewportWidth = width;
		camera2D.viewportHeight = height;
		camera2D.position.set(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, 0);
		camera2D.update();
		//camera3D.position.set(0, 0, 0);
	}
	
	public void update() {
		if (infrantic.getScreen() != null) {
			if (infrantic.getScreen() instanceof GameState) {
				float[] skyColor = ((GameState) infrantic.getScreen()).getSkyColor();
				Gdx.gl.glClearColor(skyColor[0], skyColor[1], skyColor[2], skyColor[3]);
			} else {
				Gdx.gl.glClearColor(0, 0, 0, 1);
			}
		} else {
			Gdx.gl.glClearColor(0, 0, 0, 1);
		}
		
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		camera3D.update();
		camera2D.update();
	}
	
	public void drawString(String string, float x, float y) {
		drawString(string, x, y, Color.WHITE);
	}
	
	public void drawString(String string, float x, float y, Color color) {
		globalSpriteBatch.begin();
		globalSpriteBatch.setProjectionMatrix(camera2D.combined);
		globalFont.setColor(color);
		globalFont.draw(globalSpriteBatch, string, x, y);
		globalSpriteBatch.end();
	}
	
	public TextBounds getTextBounds(String text) {
		return globalFont.getBounds(text);
	}
	
	/**
	 * Get the 3D Camera
	 * @return The 3D Camera
	 */
	public PerspectiveCamera get3DCamera() {
		return camera3D;
	}

	/**
	 * Get the 2D Camera
	 * @return The 2D Camera
	 */
	public OrthographicCamera get2DCamera() {
		return camera2D;
	}
	
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	
	
}
