package dnet.gamestudios.infrantic;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;

import dnet.gamestudios.infrantic.entity.livingentity.player.EntityPlayer;
import dnet.gamestudios.infrantic.graphics.Graphics;
import dnet.gamestudios.infrantic.gui.GameGui;
import dnet.gamestudios.infrantic.gui.touchcontrols.GuiPlayerControls;
import dnet.gamestudios.infrantic.input.FPCameraController;
import dnet.gamestudios.infrantic.state.game.GameState;
import dnet.gamestudios.infrantic.state.game.map.TestLevel;
import dnet.gamestudios.infrantic.state.menu.MainMenu;
import dnet.gamestudios.infrantic.state.menu.TitleScreen;

public class Infrantic extends Game {

	public static final String VERSION = "0.1.0 Alpha";
	public final boolean DEBUG = false;
	public final boolean DEVELOPER_MODE = false;
	private boolean mobileVersion;
	public AssetManager assetManager;
	public Graphics graphics;
	public GameGui gameGui;
	public FPCameraController controller;
	public EntityPlayer localPlayer;
	
	public TitleScreen titleScreen = new TitleScreen(this);
	public MainMenu mainMenu = new MainMenu(this);
	
	public TestLevel testLevel = new TestLevel(this);
	
	public Infrantic(boolean mobile) {
		this.mobileVersion = mobile;
		assetManager = new AssetManager();
	}
	
	@Override
	public void create() {
		graphics = new Graphics(this);
		gameGui = new GameGui(this);
		controller = new FPCameraController(this, graphics.camera3D);
		if (isMobileVersion()) {
			controller.setTouchControls(new GuiPlayerControls(this));
		}
		localPlayer = new EntityPlayer(this, 00000000001, 0, -1, 0);
		setScreen(titleScreen);
	}

	@Override
	public void setScreen(Screen screen) {
		if (screen != null) {
			if (screen instanceof GameState) {
				// TDOD On screen change here
			}
		}
		super.setScreen(screen);
	}
	
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		graphics.onResize(width, height);
		gameGui.onResize(width, height);
	}

	@Override
	public void render() {
		if (assetManager.update()) {
			graphics.update();
			if (getScreen() instanceof GameState) {
				controller.update();
				gameGui.update(Gdx.graphics.getDeltaTime());
			}
			super.render();
			if (DEVELOPER_MODE) {
				graphics.drawString("DEVELOPER MODE ENABLED - This version is not for public use", graphics.width - 440, graphics.height - 5, Color.RED);
				if (getScreen() instanceof GameState) {
					if (mobileVersion) {
						graphics.drawString("Press here to show developer menu", graphics.width - 440, graphics.height - 20, Color.RED);
					} else {
						graphics.drawString("Press F9 to show developer menu", graphics.width - 440, graphics.height - 20, Color.RED);
					}
				}
			}
		} else {
			Gdx.gl.glClearColor(0, 0, 0, 1);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
			graphics.camera2D.update();
			graphics.drawString("Loading please wait...", 20, 20);
		}
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void dispose() {
		super.dispose();
		assetManager.dispose();
	}
	
	public boolean isMobileVersion() {
		return this.mobileVersion;
	}
}
