package dnet.gamestudios.infrantic.state;

import com.badlogic.gdx.Screen;

import dnet.gamestudios.infrantic.Infrantic;

public abstract class State implements Screen {

	protected Infrantic infrantic;
	private boolean callOnceDone = false;
	
	
	public State(Infrantic infrantic) {
		this.infrantic = infrantic;
	}
	
	@Override
	public void show() {
		callOnceDone = false;
		onShow();
	}

	@Override
	public void render(float delta) {
		if (!callOnceDone) {
			callOnce();
			callOnceDone = true;
		}
		doInputCheck();
		update(delta);
		render();
	}

	@Override
	public void resize(int width, int height) {
	}

	/**
	 * Called each and every time this state is shown 
	 */
	public abstract void onShow();
	/**
	 * Called the first ever time this state is shown
	 */
	public abstract void callOnce();
	/**
	 * called each frame
	 */
	public abstract void render();
	/**
	 * called prior to each frame
	 * @param delta
	 */
	public abstract void update(float delta);
	/**
	 * Called each frame prior to a call to {@code update()}
	 */
	public abstract void doInputCheck();
}
