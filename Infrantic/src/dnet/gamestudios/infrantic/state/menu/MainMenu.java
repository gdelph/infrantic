package dnet.gamestudios.infrantic.state.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.gui.component.Button;
import dnet.gamestudios.infrantic.state.State;

public class MainMenu extends State {

	private final String background = "texture/menu/title_bg.png";
	private final String bgm = "audio/menu/ssl.mp3";
	private final String titleLogo = "texture/menu/titlelogo.png";
	private Button newGameButton;
	
	public MainMenu(Infrantic infrantic) {
		super(infrantic);
	}

	@Override
	public void hide() {
		infrantic.assetManager.get(bgm, Music.class).stop();
	}

	@Override
	public void pause() {
		infrantic.assetManager.get(bgm, Music.class).pause();
	}

	@Override
	public void resume() {
		infrantic.assetManager.get(bgm, Music.class).play();
	}

	@Override
	public void dispose() {
		infrantic.assetManager.unload(background);
		infrantic.assetManager.unload(bgm);
		infrantic.assetManager.unload(titleLogo);
	}

	@Override
	public void onShow() {
		infrantic.assetManager.load(background, Texture.class);
		infrantic.assetManager.load(bgm, Music.class);
		infrantic.assetManager.load(titleLogo, Texture.class);
		newGameButton = new Button(infrantic, 0, 20, 256, 64, "texture/menu/button.png", "Start Game");
	}

	@Override
	public void callOnce() {
		infrantic.assetManager.get(bgm, Music.class).play();
		infrantic.assetManager.get(bgm, Music.class).setLooping(true);
		infrantic.titleScreen.dispose();
	}

	@Override
	public void render() {
		infrantic.graphics.globalSpriteBatch.begin();
		infrantic.graphics.globalSpriteBatch.setProjectionMatrix(infrantic.graphics.camera2D.combined);
		infrantic.graphics.globalSpriteBatch.draw(infrantic.assetManager.get(background, Texture.class), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		infrantic.graphics.globalSpriteBatch.draw(infrantic.assetManager.get(titleLogo, Texture.class), (infrantic.graphics.getWidth() / 2) - (infrantic.assetManager.get(titleLogo, Texture.class).getWidth() / 2), (infrantic.graphics.getHeight() - infrantic.assetManager.get(titleLogo, Texture.class).getHeight()) - 20, 512, 128);
		infrantic.graphics.globalSpriteBatch.end();
		newGameButton.render(infrantic.graphics);
		
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		newGameButton.setPosition((float)(width / 2) - (newGameButton.getWidth() / 2), (float)(height / 4) - (newGameButton.getHeight() / 2));
	}

	@Override
	public void update(float delta) {
		
	}

	@Override
	public void doInputCheck() {
		int x = Gdx.input.getX();
		int y = infrantic.graphics.getHeight() - Gdx.input.getY();
		if (Gdx.input.isButtonPressed(Buttons.LEFT)) {
			if (newGameButton.containsPoint(x, y)) {
				infrantic.setScreen(infrantic.testLevel);
			}
		}
	}

}
