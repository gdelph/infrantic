package dnet.gamestudios.infrantic.state.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.state.State;

public class TitleScreen extends State {
	
	private String bg = "texture/menu/lbg.png";
	private String text = "texture/menu/text.png";
	private String sound = "audio/menu/titlesound.mp3";
	private SpriteBatch sb;
	private float textPosX = -550;
	private float counter = 0;
	
	public TitleScreen(Infrantic infrantic) {
		super(infrantic);
	}

	@Override
	public void onShow() {
		infrantic.assetManager.load(bg, Texture.class);
		infrantic.assetManager.load(text, Texture.class);
		sb = new SpriteBatch();
		sb.setProjectionMatrix(infrantic.graphics.camera2D.combined);
		infrantic.assetManager.load(sound, Sound.class);
	}

	@Override
	public void callOnce() {}
	
	@Override
	public void hide() {
		textPosX = -550;
		counter = 0;
	}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void dispose() {
		sb.dispose();
		infrantic.assetManager.unload(bg);
		infrantic.assetManager.unload(sound);
		infrantic.assetManager.unload(text);
	}

	@Override
	public void render() {
		sb.begin();
		sb.setProjectionMatrix(infrantic.graphics.camera2D.combined);
		sb.draw(infrantic.assetManager.get(bg, Texture.class), (Gdx.graphics.getWidth() / 2) - 128, (Gdx.graphics.getHeight() / 2) - 128, 256, 256);
		sb.draw(infrantic.assetManager.get(text, Texture.class), ((Gdx.graphics.getWidth() / 2) - 128)  + textPosX, ((Gdx.graphics.getHeight() / 2) - 32), 256, 64);
		sb.end();
		infrantic.graphics.drawString("Created 2013 � DNET - Guy Delph", 5, 20);
	}

	@Override
	public void update(float delta) {
		if (textPosX < 0) {
			textPosX += delta * 200;
		} else if (textPosX > 0) {
			//sound.play();
			infrantic.assetManager.get(sound, Sound.class).play();
			textPosX = 0;
		} else {
			counter += delta * 2;
			//System.out.println(counter);
			if (counter > 5) {
				infrantic.setScreen(infrantic.mainMenu);
				//counter = 0;
				//textPosX = -550;
			}
		}
	}

	@Override
	public void doInputCheck() {}

}
