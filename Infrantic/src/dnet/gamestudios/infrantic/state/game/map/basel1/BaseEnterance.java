package dnet.gamestudios.infrantic.state.game.map.basel1;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.state.game.GameState;

public class BaseEnterance extends GameState {

	public BaseEnterance(Infrantic infrantic) {
		super(infrantic, 0);
	}

	@Override
	public void onShow() {
		
	}

	@Override
	public void callOnce() {
		
	}
	
	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}

	@Override
	public void render3D() {
		
	}

	@Override
	public void render2D() {
		
	}

	@Override
	public void update(float delta) {
		
	}

	@Override
	public void doInputCheck() {
		
	}

}
