package dnet.gamestudios.infrantic.state.game.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.math.Intersector;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.entity.EntityCubicMarker;
import dnet.gamestudios.infrantic.graphics.model.Model;
import dnet.gamestudios.infrantic.graphics.model.basicparts.ModelWall;
import dnet.gamestudios.infrantic.gui.developer.DeveloperGui;
import dnet.gamestudios.infrantic.state.game.GameState;

public class TestLevel extends GameState {
	
	private Mesh mesh;
	private Model modelWall, modelWall1, modelWall2;
	private EntityCubicMarker marker;
	public TestLevel(Infrantic infrantic) {
		super(infrantic, 3);
	}

	@Override
	public void onShow() {
		Gdx.input.setInputProcessor(infrantic.controller);
		if (mesh == null) {
			mesh = new Mesh(true, 2, 0, new VertexAttribute(Usage.Position, 3, "a_position"));
			mesh.setVertices(new float[] {
					0, -1, 0,
					0, -1, 100,
			});
		}
		modelWall = new ModelWall(infrantic, "texture/model/brick.png", 5, -1, 5, 8, 2, 1);
		modelWall.load();
		modelWall1 = new ModelWall(infrantic, "texture/model/brick.png", 13, -1, 5, 1, 2, 8);
		modelWall1.load();
		modelWall2 = new ModelWall(infrantic, "texture/model/brick.png", 5, -1, 5, 1, 2, 8);
		modelWall2.load();
		super.sceneObjects[0] = modelWall;
		super.sceneObjects[1] = modelWall1;
		super.sceneObjects[2] = modelWall2;
		marker = new EntityCubicMarker(021545, 9, -1, 10);
		marker.setColor(1, 0, 0);
		super.entities.add(marker);
		//infrantic.localPlayer.putAt(5, -1, 5);
	}

	@Override
	public void callOnce() {
		Gdx.input.setCursorCatched(true);
	}
	
	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}

	@Override
	public void render3D() {
		Gdx.gl10.glColor4f(0, 1, 0, 1);
		for (int xP = 0; xP < 100; xP +=1) {
				Gdx.gl10.glPushMatrix();
				Gdx.gl10.glTranslatef(xP, 0, 0);
				mesh.render(GL10.GL_LINES);
				Gdx.gl10.glPopMatrix();
		}
		Gdx.gl10.glTranslatef(100, 0, 0);
		Gdx.gl10.glRotatef(-90, 0, 1, 0);
		for (int zP = 0; zP < 100; zP +=1) {
			Gdx.gl10.glPushMatrix();
			Gdx.gl10.glTranslatef(zP, 0, 0);
			mesh.render(GL10.GL_LINES);
			Gdx.gl10.glPopMatrix();
		}
		Gdx.gl10.glRotatef(90, 0, 1, 0);
		Gdx.gl10.glTranslatef(-100, 0, 0);
		Gdx.gl10.glColor4f(1, 1, 1, 1);
		super.renderSceneObjects();
	}

	@Override
	public void render2D() {
		infrantic.graphics.drawString("FPS: " + Gdx.graphics.getFramesPerSecond(), 20, infrantic.graphics.getHeight() - 10);
	}

	boolean t = false;
	
	@Override
	public void update(float delta) {
		super.updateEntities(delta);
		//System.out.println("X: " + infrantic.graphics.camera3D.direction.x + " Y:" + infrantic.graphics.camera3D.direction.z);
	}
	
	boolean up = false;
	@Override
	public void doInputCheck() {
		if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
			Gdx.input.setCursorCatched(false);
		}
		if (Gdx.input.isButtonPressed(Buttons.LEFT)) {
			if (!Gdx.input.isCursorCatched()) {
				Gdx.input.setCursorCatched(true);
			}
		}
		if (Gdx.input.isKeyPressed(Keys.F9)) {
			infrantic.gameGui.addGui("Devloper", new DeveloperGui(infrantic, 20, 20, Gdx.graphics.getWidth() - 40, Gdx.graphics.getHeight() - 50));
		}
	}
}
