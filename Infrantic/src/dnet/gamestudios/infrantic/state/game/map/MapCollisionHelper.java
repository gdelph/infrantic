package dnet.gamestudios.infrantic.state.game.map;

import com.badlogic.gdx.math.Intersector;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.graphics.model.Model;
import dnet.gamestudios.infrantic.state.game.GameState;

public class MapCollisionHelper {

	public static final int FORWARD = 0;
	public static final int BACKWARDS = 1;
	public static final int LEFT = 2;
	public static final int RIGHT = 3;
	public static final int UP = 4;
	public static final int DOWN = 5;
	
	private Infrantic infrantic;
	
	public MapCollisionHelper(Infrantic infrantic) {
		this.infrantic = infrantic;
	}
	
	public boolean canMove(int direction, GameState map) {
		boolean movePossible = true;
		Model[] temp = map.getSceneObjects();
		switch (direction) {
			case FORWARD:
				for (int i = 0; i < temp.length;  i++) {
					if (Intersector.intersectRayBoundsFast(infrantic.controller.getRay(), temp[i].getBounds())) {
						if (infrantic.localPlayer.getBounds().intersects(temp[i].getBounds())) {
							movePossible = false;
							break;
						} else {
							
						}
					}
				}
				break;
			case BACKWARDS:
				for (int i = 0; i < temp.length;  i++) {
					if (infrantic.localPlayer.getBounds().intersects(temp[i].getBounds())) {
						
					}
				}
				break;
			case LEFT:
				break;
			case RIGHT:
				break;
			case UP:
				break;
			case DOWN:
				break;
			default:
				movePossible = true;
				break;
		}
		return movePossible;
	}
	
	public void update() {
		
	}
	
}
