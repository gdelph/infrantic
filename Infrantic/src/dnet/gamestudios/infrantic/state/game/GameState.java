package dnet.gamestudios.infrantic.state.game;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;

import dnet.gamestudios.infrantic.Infrantic;
import dnet.gamestudios.infrantic.entity.Entity;
import dnet.gamestudios.infrantic.graphics.model.Model;
import dnet.gamestudios.infrantic.state.State;

public abstract class GameState extends State {
	
	protected Model[] sceneObjects;
	protected ArrayList<Entity> entities;
	
	public GameState(Infrantic infrantic, int numberOfSceneObjects) {
		super(infrantic);
		this.sceneObjects = new Model[numberOfSceneObjects];
		this.entities = new ArrayList<Entity>();
	}

	@Override
	public void render() {
		Gdx.gl10.glViewport(0, 0, infrantic.graphics.getWidth(), infrantic.graphics.getHeight());
		Gdx.gl10.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		infrantic.graphics.camera3D.apply(Gdx.gl10);
		Gdx.gl10.glEnable(GL10.GL_TEXTURE_2D);
		Gdx.gl10.glEnable(GL10.GL_CULL_FACE);
		Gdx.gl10.glCullFace(GL10.GL_FRONT);
		Gdx.gl10.glEnable(GL10.GL_DEPTH_TEST);
		render3D();
		Gdx.gl10.glDisable(GL10.GL_CULL_FACE);
		Gdx.gl10.glDisable(GL10.GL_DEPTH_TEST);
		if (infrantic.controller != null) {
			if (infrantic.controller.touchOverlay != null) {
				infrantic.controller.touchOverlay.render();
			}
		}
		infrantic.gameGui.render(infrantic.graphics);
		render2D();
	}

	public abstract void render3D();
	public abstract void render2D();
	
	protected void renderSceneObjects() {
		for (int i = 0; i < sceneObjects.length; i++) {
			sceneObjects[i].render(infrantic.graphics);
		}
		for (int i = 0; i < entities.size(); i ++) {
			entities.get(i).render(infrantic.graphics);
		}
	}
	
	protected void updateEntities(float delta) {
		for (int i = 0; i < entities.size(); i ++) {
			entities.get(i).update(delta);
		}
	}
	
	public Model[] getSceneObjects() {
		return this.sceneObjects;
	}
	
	public float[] getSkyColor() {
		return new float[] {0, 0, 0, 1};
	}
	
}
