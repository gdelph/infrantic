package dnet.gamestudios.infrantic.game.world;

import dnet.gamestudios.infrantic.graphics.model.Model;

public interface World {

	public Model[] getModels();
	public float[] getSkyColor();
	
}
